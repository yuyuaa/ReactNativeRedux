import React,{Component,View,StyleSheet,Text,ListView,LayoutAnimation,UIManager } from 'react-native';
import Bw from './bw';

export default class bwlist extends Component {
  static propTypes = {
  };
  componentWillMount(){
  }
  constructor(props) {
    super(props);
    //LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.state={
      dataSource:['高','梁','南','奉','大','高','梁','南','奉','大'],
      //一开始是第一个高亮的
      ifSelect:[1,0,0,0,0]
    }
  }
  //选择每一个list时的回调
  _selectItem(e,index){
    //alert("222222")
    //选择的项目高亮显示
    var temp=[0,0,0,0,0];
    temp[index]=1;
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({
      ifSelect:temp
    });
    //console.log('你选择了项目'+index);
  }
  onSelect=()=>{
    this.props.navigator.push({
      name: 'bwList'
    });
  };
  render() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
      <ListView
        style={styles.contain}
        dataSource={ds.cloneWithRows(this.state.dataSource)}
        renderRow={(rowData,sec,index) => <Bw
                                            onSelect={this.onSelect}
                                            shortname={rowData} 
                                            index={index}>
                                          </Bw>} 
        //renderHeader={(e)=>this._renderHeader(e)}
      />
        
    );
  }
}

const styles = StyleSheet.create({
  contain:{
    flex:1,
    //backgroundColor:'red',

  }
});