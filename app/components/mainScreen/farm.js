import React,{Component,View,StyleSheet,Text,UIManager,LayoutAnimation,TouchableOpacity} from 'react-native';
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
export default class farm extends Component {
  static propTypes = {
  };

  constructor(props) {
    super(props);
    this.state={
      ifSelect:false//初始化时不是选择状态
    }
  }
  onclick = ()=>{
    this.toSelectState();
    var self=this;
    //调用父组件方法
    this.props.selectItem(self.props.index);
  };
  //转为选择状态
  toSelectState = ()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({
      isSelect:true
    })
  };
  //转为不选择状态
  toNoSelectState=()=>{
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({
      isSelect:false
    })
  };
  render() {
    //alert(this.props.index);
    return (
      <TouchableOpacity onPress={this.onclick}>
      <View style={styles.contain} >
          {/*圆形里面放字*/}
          <View style={[styles.circle,this.state.isSelect ? styles.SelectCircle : styles.noSelectCircle]} >
            <Text  style={this.state.isSelect ? styles.SelectShortname : styles.noSelectShortname}>{this.props.shortname}</Text>
          </View>
      </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  contain:{
    flex: 1,
    //height:70,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'red',
    padding: 2,
    //marginLeft: 6,
    //marginRight: 6,
    marginVertical: 4,
  },
  //无论选中还是没选中通用的样式
  circle:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth:0.5,
    borderColor:'#eeeeee',
  },
  //没有选中时的样式
  noSelectCircle:{
    //flex: 1,
    width:46,
    height:46,
    borderRadius:23,
    backgroundColor:'#2b90d9',
    elevation:5
  },
  //选中时的样式
  SelectCircle:{
    //flex: 1,
    width:60,
    height:60,
    borderRadius:30,
    backgroundColor:'#80deea',
    //安卓shadow
    elevation:20
  },
  //当前为选择项目时的字体大小
  noSelectShortname:{
    fontSize:25,
    color:'white',
    textAlign:'center'
  },
  //当前为不是选择项目时的字体大小
  SelectShortname:{
    fontSize:32,
    color:'white',
    textAlign:'center'
  },
});