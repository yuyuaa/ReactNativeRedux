import React,{Component,View,Text,StyleSheet,TouchableNativeFeedback} from 'react-native';

export default class bw extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
  }
  _onPressButton=()=>{
  	this.props.onSelect();
  };
  render() {
    return (
    	<TouchableNativeFeedback onPress={this._onPressButton}>
    	<View style={styles.contain}>
    		<View style={styles.left}>
    			<Text  style={styles.leftText}>哺乳</Text>
    			<Text  style={styles.leftText}>2日</Text>
    		</View>
    		<View style={styles.right}>
    			<Text>控制器:0000000000000</Text>
    			<Text>母猪号:BS12-999999</Text>
    			<View style={styles.lastText}>
    				<Text style={styles.lastTextLeft}>已吃:23克</Text>
    				<View style={styles.onlineOrNot}><Text style={styles.onlineOrNotText}>在线</Text></View>
    			</View>
    		</View>
    	</View>
    	</TouchableNativeFeedback>
    );
  }
}

var styles = StyleSheet.create({
  contain:{
    flex: 1,
    //height:70,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 2,
    marginLeft: 8,
    marginRight: 8,
    marginVertical: 5,
    //安卓shadow
    elevation:10,
    borderRadius:4,
    borderColor:'#dddddd',
    borderWidth:1
  },
  left:{
  	flex:1,
  	//backgroundColor:'yellow',
  	flexDirection:'column',
  	justifyContent:'center',
  	alignItems:'center',
  	borderRightWidth:1,
  	borderRightColor:"#CBC5C5"
  },
  leftText:{
  	fontSize:20
  },
  right:{
  	flex:4,
  	paddingLeft:7,
  	//backgroundColor:'green',
  },
  lastText:{
  	flex:1,
  	//backgroundColor:'red',
  	flexDirection:'row'
  },
  lastTextLeft:{
  	flex:5,
  	//backgroundColor:'yellow'
  },
  onlineOrNot:{
  	flex:1,
  	backgroundColor:'#5ABF15',
  	marginRight:4,
  	marginBottom:3,
  	justifyContent:'center',
  	alignItems:'center',
  	borderRadius:4
  },
  onlineOrNotText:{
  	fontSize:13,
  	color:'white'
  }
});
