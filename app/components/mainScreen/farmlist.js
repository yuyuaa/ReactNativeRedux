import React,{Component,View,StyleSheet,Text,ListView} from 'react-native';
import Farm from './farm';
export default class farmlist extends Component {
  static propTypes = {
  };
  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state={
      dataSource:ds.cloneWithRows(['高','梁','南','奉','大','高','高','梁','南','奉','大','高']),
    };
    //存放list的引用
    this.mylist=[];
    this.selectItemIndex=0;//被选中的list项目初始是0
  }
  //首次render后
  componentDidMount(){
    //默认选中第一个
    this.mylist[this.selectItemIndex].toSelectState();
  }
  //选择每一个list时的回调
  _selectItem = (index)=>{
    if (index==this.selectItemIndex) {
      return;
    }else{
      this.mylist[this.selectItemIndex].toNoSelectState();
      this.mylist[index].toSelectState();
      this.selectItemIndex=index;
    } 
  };
  render() {
    return (
      <ListView
        style={styles.contain}
        dataSource={this.state.dataSource}
        renderRow={(rowData,sec,index) => <Farm
                                            ref={(row) =>{this.mylist[index]=row;}}
                                            shortname={rowData} 
                                            index={index} 
                                            selectItem={this._selectItem}>
                                          </Farm>} 
        //renderHeader={(e)=>this._renderHeader(e)}
      />
        
    );
  }
}

const styles = StyleSheet.create({
  contain:{
    flex:1,
    //backgroundColor:'red',

  }
});