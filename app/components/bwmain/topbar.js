import React,{Component,View,StyleSheet,Text} from 'react-native';
import FontIcon from 'react-native-vector-icons/FontAwesome';

export default class topbar extends Component {
  static propTypes = {
    name: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
  }
  popNavigator=()=>{
    this.props.navigator.pop();
  };
  render() {
    return (
      <View style={styles.topbar} >
        {/*返回图标*/}
      	<Text style={styles.iconLeft} onPress={this.popNavigator}>
          <FontIcon name="arrow-left" size={25} color="white" />
        </Text>
        {/*中间部分文字*/}
        <View style={styles.title}>
          <Text style={styles.textfarm}>高村猪场/一线分娩2</Text>
          <Text style={styles.textmac}>机器号:20000000000</Text>
        </View>
        <Text style={styles.iconLeft}>
          <FontIcon name="cog" size={25} color="white" />
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  topbar:{
  	flex:2,
  	backgroundColor:"#113477",
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:"center",
    //安卓的shadow 效果只在5.0以上有效
    elevation:2
  },
  iconLeft:{
    //backgroundColor:'red',
    flex:2,
    justifyContent:'center',
    alignItems:"center",
    textAlign:'center'
  },
  title:{
    flex:10,
    justifyContent:'center',
    //backgroundColor:'yellow'
  },
  textfarm:{
    textAlign:"center",
    color:'white'
  },
  textmac:{
    textAlign:"center",
    color:'white'
  }
});