import React, { Component } from 'react-native';
import Topbar from '../components/bwmain/topbar'

const {
  StyleSheet,
  View,
  Text,
  ViewPagerAndroid,
  TouchableOpacity
} = React;



class bwmain extends Component {
	constructor(props) {
	    super(props);
	    //LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
	    this.state={
	    	current:0
	    }
	 }

	render() {
	    return (
	      <View style={styles.contain}>
	      	<View style={styles.topbar}><Topbar navigator={this.props.navigator}></Topbar></View>
	      {/*tab 指示3个*/}
	      	<View style={styles.tabtip}>
	      		<TouchableOpacity  
	      			style={[styles.tabtipView,{borderBottomWidth:2}]} 
	      			onPress={()=>{alert("2222")}}
	      		>
	      			<Text style={styles.tabtipText}>图表</Text>
	      		</TouchableOpacity>
	      		<TouchableOpacity style={styles.tabtipView}>
	      			<Text style={styles.tabtipText}>日下料</Text>
	      		</TouchableOpacity>
	      		<TouchableOpacity style={styles.tabtipView}>
	      			<Text style={styles.tabtipText}>详细下料</Text>
	      		</TouchableOpacity>
	      	</View>
	      	<ViewPagerAndroid style={styles.viewpager} initialPage={0}>
		        <View style={styles.pageStyle}>
		        	<Text>First page</Text>
		        </View>
		        <View style={styles.pageStyle}>
		        	<Text>Second page</Text>
		        </View>
		        <View style={styles.pageStyle}>
		        	<Text>Second page</Text>
		        </View>
			</ViewPagerAndroid>
	      </View>
	    );
	}
}

var styles = StyleSheet.create({
	contain:{
		flex:1
	},
	topbar:{
		flex:1,
		//backgroundColor:'red'
	},
	viewpager:{
		flex:10,
		//backgroundColor:'green'
	},
	tabtip:{
		flex:0.7,
		//backgroundColor:'red',
		flexDirection:'row',
		justifyContent:'center',
		alignItems:'flex-end',
		borderBottomWidth:0.5
	},
	tabtipView:{
		flex:1,
	},
	tabtipText:{

		textAlign:'center',
		fontSize:20
	},
	  pageStyle: {
	    alignItems: 'center',
	    padding: 20,
	  }
});


export default bwmain;
