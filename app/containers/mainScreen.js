import React,{Component,View,StyleSheet,Navigator,Text,BackAndroid} from 'react-native';
import Topbar from '../components/mainScreen/topbar';
import FarmList from '../components/mainScreen/farmlist';
import BwList from '../components/mainScreen/bwlist';
import BwMain from './bwmain'

var _navigator;
BackAndroid.addEventListener('hardwareBackPress', function() {
  if (_navigator && _navigator.getCurrentRoutes().length > 1) {
    _navigator.pop();
    return true;
  }
  return false;
});

export default class mainScreen extends Component {
  static propTypes = {
    name: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
  }


  //路由映射
  RouteMapper=(route, navigator, onComponentRef)=>{
    _navigator=navigator;
    //主页面
    if (route.name==='mainScreen') {
      return(
        <View style={styles.contain}>
          <View style={styles.topbar}><Topbar openDrawer={this.props.openDrawer}></Topbar></View>
          <View style={styles.farmlistAndbwlist}>
            <View style={styles.bwlist}><BwList navigator={navigator}></BwList></View>
            {/*右侧猪场列表*/}
            <View style={styles.farmlist}><FarmList></FarmList></View>
          </View>
        </View>
      )
    };
    //单击每一个bwlist的时候跳转
    if (route.name==='bwList') {
      return(
        <BwMain navigator={navigator}></BwMain>
      )
    };
  };
  render() {
    return (
      <Navigator
        style={{flex:1}}
        initialRoute={{name: 'mainScreen'}}
        configureScene={() => Navigator.SceneConfigs.FadeAndroid}//转场动画
        renderScene={this.RouteMapper}
      />
    );
  }
}

const styles = StyleSheet.create({
  contain:{
  	flex:1,
  	//backgroundColor:"red"
  },
  topbar:{
  	flex:1,
  	//backgroundColor:"green"
  },
  farmlistAndbwlist:{
  	flex:10,
  	//backgroundColor:"yellow",
  	flexDirection:"row"
  },
  farmlist:{
  	flex:3,
    borderStyle: null,
    borderRightColor:'grey',
    borderLeftWidth:0.5
  	//backgroundColor:"blue"
  },
  bwlist:{
  	flex:10,
  	backgroundColor:"#CFE2EB"
  }
});