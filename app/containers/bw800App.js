'use strict';

import React, { Component,DrawerLayoutAndroid,View,Text} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';
import MainScreen from './mainScreen'

// @connect(state => ({
//   state: state.counter
// }))
 export default class bw800App extends Component {
  constructor(props) {
    super(props);
  }
  openDrawer= ()=> {
    this.refs['DRAWER'].openDrawer()
  };
  render() {
    var navigationView = (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>I'm in the Drawer!</Text>
      </View>
    );
    return (

      <DrawerLayoutAndroid
          //打开时的抽屉宽度
          drawerWidth={300}
          ref={'DRAWER'}
          //左边弹出
          drawerPosition={DrawerLayoutAndroid.positions.Left}
          //弹出的页面
          renderNavigationView={() => navigationView}>
            {/*不显示抽屉时的页面*/}
            <View style={{flex: 1,alignSelf:'stretch'}}>
              <MainScreen openDrawer={this.openDrawer}></MainScreen>
            </View>
      </DrawerLayoutAndroid>
    );
  }
}
