## ReactNative  Bw800控制器用app


![](https://git.oschina.net/yuyuaa/ReactNativeRedux/raw/master/README%E6%88%AA%E5%9B%BE/master%E5%88%86%E6%94%AF%E6%95%88%E6%9E%9C%E5%9B%BE.jpg?dir=0&filepath=README%E6%88%AA%E5%9B%BE%2Fmaster%E5%88%86%E6%94%AF%E6%95%88%E6%9E%9C%E5%9B%BE.jpg&oid=3b52cd7558aa4e028bf20146f22647662eaafd9d&sha=4a5e9d6880b2f6da20b0ac22a650d76ce85ca0a8)

#### React-Native 0.21.0 & Redux 3.3.1 & React-Redux 4.4.0

This repo demonstrates the usage of latest React-Native with Redux.

### 依赖
1.nodejs，npm，react-native-cli

2.安装andriod SDK并配置环境变量

3.一个andriod 模拟器

### 使用

- you have to use `npm@3.x`, if you are using `npm@2.x` you might get into some wired scenarios. Please open an issue if you can't get it run on `npm@2.x`. also you can follow this [issue](https://github.com/rackt/react-redux/issues/236) for more info.
- clone the project
- go to `Counter` folder
- run `npm install`
- 进入andriod文件下的local.properties修改Andriod sdk的文件目录为自己的
- 打开andriod模拟器或连接手机
- 运行**react-native run-android**
- 运行**react-native start** 
Cheers,

### 一.字体插件使用

[github react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)

使用时，先`npm install react-native-vector-icons --save` 在`rnpm link` 再运行`react-native run-android`

### 二.~~listView互斥选择效果的实现(已放弃使用，这种方法在渲染多个list时会更新全部list会有卡屏的感觉)~~

1.在listView中具有一个数组，这个数组代表下面的item哪个使用，选择时的样式，哪个渲染不选择时的样式

2.listView在renderRow中存入这个样式状态，并且定义一个单击事件，这个事件会回调listview中的函数，listview中的函数会动态设置数组
```javascript
	//用listview的数组渲染每一个子项目
	renderRow={(rowData,sec,index) => <Farm
                                            ifSelect={this.state.ifSelect[index]} 
                                            shortname={rowData} 
                                            index={index} 
                                            selectItem={(e,inedx)=>this._selectItem(e,index)}>
                                          </Farm>} 

    //数组
    this.state={
      dataSource:['高','梁','南','奉','大'],
      //一开始是第一个高亮的
      ifSelect:[1,0,0,0,0]
    }

    //回调函数动态设置数组
     //选择每一个list时的回调
	 _selectItem(e,index){
	    //alert("222222")
	    //选择的项目高亮显示
	    var temp=[0,0,0,0,0];
	    temp[index]=1;
	    this.setState({
	      ifSelect:temp
	    });
	    //console.log('你选择了项目'+index);
	  }
```
### 三.listView互斥选择效果的实现(每次改动只渲染2个list)

1.给每一个list的状态添加一个变量代表选择或者没有选择的状态。

2.list向listview暴露2个方法，控制list状态之间的切换

3.listview会记录当前所有list中的选择的项目的refs,list初始化时用ref 标记具体可以看笔记

4.当新list单击时，先将自身调用listview的方法，该方法会通过引用来操作2个list分别是前一个选择的list变为不是选择的状态，新选择的list变为选择状态

具体可以回退到 commit ”不会卡的listview互斥选择“ 2016-04-10 参考代码

### 四.使用LayoutAnimation让状态设置导致的渲染变得平滑

![](https://git.oschina.net/yuyuaa/Bw800App/raw/master/README%E6%88%AA%E5%9B%BE/%E4%BD%BF%E7%94%A8LayoutAnimation%E5%8A%A8%E7%94%BB.gif?dir=0&filepath=README%E6%88%AA%E5%9B%BE%2F%E4%BD%BF%E7%94%A8LayoutAnimation%E5%8A%A8%E7%94%BB.gif&oid=e26e6ab18793293b2e445a8f542a204a638c942f&sha=ade408d39712aaa1ffe3172dd45220d8ad4d124c)

[参考](https://facebook.github.io/react-native/docs/animations.html)

** 1.如果在安卓中使用，需要在import后面直接加上这句 **
```javascript
  import React,{Component,View,StyleSheet,Text,ListView,LayoutAnimation,UIManager } from 'react-native';
  import Farm from './farm';
  UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
```

**  2.使用时直接在setState前面加上这句，就会在下次的渲染周期加上动画 **
```javascript
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({
      ifSelect:temp
    });
```
```javascript
```

### 四.手机返回键可以pop路由
参考commit“手机返回键pop路由” 2016-04-18

```javascript
  var _navigator;
  BackAndroid.addEventListener('hardwareBackPress', function() {
    if (_navigator && _navigator.getCurrentRoutes().length > 1) {
      _navigator.pop();
      return true;
    }
    return false;
  });

  RouteMapper=(route, navigator, onComponentRef)=>{
    _navigator=navigator;
```